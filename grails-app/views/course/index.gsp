<%--
  Created by IntelliJ IDEA.
  User: tnguyen
  Date: 20/05/2020
  Time: 16:49
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Course Index</title>
</head>

<body>
    <div class="row">
        <div class="col-12 xs-12">
            <h2 class="display-4">Two Command Objects</h2>
            <g:form name="submit-author-books" controller="course" action="createAuthor">
                <g:fieldValue name="fullName" value="" field="" bean=""/>
                <g:fieldValue bean="" field="" name="books[0].title" value=""/>
                <g:fieldValue name="books[0].isbn" value="" bean="" field=""/>
                <g:fieldValue  bean="" field="" name="books[1].title" value=""/>
                <g:fieldValue  bean="" field="" name="books[1].isbn" value=""/>
            </g:form>
        </div>
    </div>
</body>
</html>
